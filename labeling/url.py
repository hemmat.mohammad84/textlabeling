from django.urls import path
from . import views


urlpatterns = [
        path('textprocessing/textlabeling', views.start),
]