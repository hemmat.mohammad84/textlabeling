from django.conf import settings
import string
import numpy as np
class TextLabeling():

	def __init__(self, data):
		self.model = settings.MODEL
		# self.connection = settings.CONNCETION
		self.category = data['cats']
		self.news = data['text']



	def preprocessing(self):

		doc = self.model(self.news.lower())
		doc = [word for word in doc if not word.is_stop and str(word) not in string.punctuation]
		return self.find_similarity(doc)

	def find_similarity(self, data):
		similarity = dict()
		temp_score = []
		total = 0
		for keys in self.category:
			for element in self.category[keys]:
				token = self.model(element)
				for word in data:
					if word.has_vector and token.has_vector:
						sim = word.similarity(token)

						if sim>0.59:
							temp_score.append(sim)
					else:

						pass
			if temp_score:

						similarity[keys] = (np.mean(temp_score) * len(temp_score)) / len(self.category[keys])
						# print(similarity[keys])
						total+=similarity[keys]
						temp_score = []
			else:
				similarity[keys] = 0
		# print(similarity)
		for keys in similarity:
			if total == similarity[keys]:
				similarity[keys] = round(similarity[keys] * 100)
			else:
				similarity[keys] = round(similarity[keys] / total * 100)
		return similarity
				# 	temp_score.append(word1.similarity(word12))
				# similarity[j] = temp_score
				# temp_score = []




