from labeling.models import CategoryKeyword, ArmyCategory, ArmyKeyword
from  django.conf import settings
class DB_CATEGORY:

    def __init__(self):
        pass
        # conn = settings.CONNCETION
        #
        # self.cursor = conn.cursor()


    def make_cat(self):
        try:
            # print('heere')
            output = {}
            find_keyword = "select id, label from army.army_keyword"
            find_category = "select id, label from army.army_category"
            key2cat = "select army_category_id, army_keyword_id from army.category_keyword"

            cats = [(item.id, item.label) for item in ArmyCategory.objects.all()]
            keywords = [(item.id, item.label) for item in ArmyKeyword.objects.all()]
            rel = [(i.army_category_id.id, i.army_keyword_id.id) for i in CategoryKeyword.objects.all()]
            # print(cats)
            # print(keywords)
            # print(rel)
            for i in cats:
                temp = []
                for j in rel:
                    if i[0] in j:

                        for k in keywords:
                            if j[1] in k:
                                temp.append(k[1])
                output[i[1]] = temp
            return output
        except Exception as e:
            return str(e)

    # def execute_query(self, query):
    #     try:
    #         self.cursor.execute(query)
    #         rows = self.cursor
    #         return rows
    #     except Exception as e:
    #         return str(e)
