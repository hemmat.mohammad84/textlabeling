from django.db import models
from django.contrib.postgres.fields import ArrayField

class News(models.Model):
	_id = models.CharField(max_length=70)
	title = models.CharField(max_length=70)
	body = models.TextField()
	source = models.CharField(max_length=70)
	agency_id = models.BigIntegerField()
	date = models.DateTimeField()
	link = models.CharField(max_length=250)
	created_at = models.DateTimeField()
	authors = ArrayField(models.CharField(max_length=70))

	class Meta:
		managed = False
		db_table = 'news'


class ArmyCategory(models.Model):
	label = models.CharField(max_length=70)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		managed = False
		db_table = 'army_category'

class ArmyKeyword(models.Model):
	label = models.CharField(max_length=70)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		managed = False
		db_table = 'army_keyword'


class CategoryKeyword(models.Model):
	army_category_id = models.OneToOneField('ArmyCategory', related_name='category',
	                                        related_query_name='category_id', on_delete=models.CASCADE,
	                                        db_column='army_category_id')


	army_keyword_id = models.OneToOneField('ArmyKeyword', related_name='keyword',
	                                        related_query_name='keyword_id', on_delete=models.CASCADE,
	                                        db_column='army_keyword_id')
	class Meta:
		managed = False
		db_table = 'category_keyword'

class NewsCategory(models.Model):
	news_id = models.OneToOneField('News', related_name='news_ids', related_query_name='news_id',
	                               on_delete=models.CASCADE, db_column='news_id')


	army_category_id = models.OneToOneField('ArmyCategory', related_name='cat_ids', related_query_name='cat_id',
	                               on_delete=models.CASCADE, db_column='army_category_id')

	score = models.FloatField(null=True, blank=True, default=None)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		managed = False
		db_table = 'news_category'

