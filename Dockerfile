FROM python:3.6.7



RUN pip install django numpy spacy psycopg2
RUN python -m spacy download en_core_web_lg
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
ADD . /usr/src/app

EXPOSE 8000
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]


